package id.co.nexsoft.login.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FormatData {
    private static final String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
    private static final String[] phoneRegex = {"[^0-9+]", "\\+$"};
    private static final String codeCountry = "+62";
    
    public static String formatPhoneNumber(String phoneNumber) {
        String numericOnly = phoneNumber.replaceAll(phoneRegex[0], "");
        numericOnly = numericOnly.replaceFirst(phoneRegex[1], "");

        if (numericOnly.startsWith(codeCountry)) {
            return numericOnly;
        }
        return codeCountry + numericOnly.substring((numericOnly.charAt(0) == '0') ? 1 : 0);
    }

    public static boolean isValidEmail(String email) {
        Pattern pattern = Pattern.compile(emailRegex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
