package id.co.nexsoft.login.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.login.model.User;

public interface UserRepository extends CrudRepository<User, Long> {
    List<User> findAll();
    Optional<User> findById(Long id);
    Optional<User> findByEmail(String email);
    Optional<User> findByPhone(String phone);
}
