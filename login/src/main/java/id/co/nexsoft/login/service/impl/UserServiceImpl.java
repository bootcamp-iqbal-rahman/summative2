package id.co.nexsoft.login.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import id.co.nexsoft.login.model.User;
import id.co.nexsoft.login.repository.UserRepository;
import id.co.nexsoft.login.service.DefaultService;
import id.co.nexsoft.login.util.GetPassword;

@Service
public class UserServiceImpl implements DefaultService<User> {
    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> getAllData() {
        return userRepository.findAll();
    }

    @Override
    public Optional<User> getDataById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public ResponseEntity<String> saveData(User data,
            BindingResult result) {
        if (result.hasErrors()) {
            List<String> errorMessages = result.getAllErrors().stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.toList());

            return ResponseEntity.badRequest().body(errorMessages.get(0));
        }
        
        data.setPassword(GetPassword.hashPassword(data.getPassword()));
        userRepository.save(data);
        return ResponseEntity.ok("Validation successful");
    }

    @Override
    public void updateData(User data, Long id) {
        Optional<User> userList = userRepository.findById(id);
        
        if(!userList.isPresent()) {
            return;
        }

        data.setId(id);
        userRepository.save(data);
    }

    @Override
    public void deleteData(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public Boolean checkUsername(String email) {
        Optional<User> userList = userRepository.findByEmail(email);
        if(userList.isPresent()) {
            return false;
        }

        Optional<User> user = userRepository.findByPhone(email);
        if(user.isPresent()) {
            return false;
        }
        return true;
    }

}
