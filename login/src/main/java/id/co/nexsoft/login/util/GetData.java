package id.co.nexsoft.login.util;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class GetData {
    private static final String apiUrl = "http://localhost:8080/";

    public static HttpResponse<String> getData(String urlString) {
        String url = apiUrl + urlString;

        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .GET()
                .build();

        try {
            HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            return httpResponse;
        } catch (Exception e) {
            return null;
        }
    }

    public static HttpResponse<String> postSimpleData(String urlString, String body) {
        String url = apiUrl + urlString;

        try {
            HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .uri(URI.create(url))
                .header("Content-Type", "application/json")
                .build();

            HttpResponse<String> response = HttpClient.newHttpClient()
                .send(request, HttpResponse.BodyHandlers.ofString());
            return response;
        } catch (Exception e) {
            return null;
        }
    }

    public static HttpResponse<String> putData(String urlString, String requestBody) {
        String url = apiUrl + urlString;
        
        try {
            HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .uri(URI.create(url))
                .header("Content-Type", "application/json")
                .build();

            HttpResponse<String> response = HttpClient.newHttpClient()
                .send(request, HttpResponse.BodyHandlers.ofString());
            return response;
        } catch (Exception e) {
            return null;
        }
    }
}
