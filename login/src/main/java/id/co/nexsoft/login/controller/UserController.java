package id.co.nexsoft.login.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.login.model.User;
import id.co.nexsoft.login.service.DefaultService;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private DefaultService<User> defaultService;
    
    @GetMapping
    public List<User> getAllUser() {
        return defaultService.getAllData();
    }

    @GetMapping("/{id}")
    public User getUserById(@PathVariable Long id) {
        return defaultService.getDataById(id).get();
    }

    @PostMapping
    public ResponseEntity<String> saveUser(@Valid @RequestBody User data,
            BindingResult result) {
        return defaultService.saveData(data, result);
    }
    
    @PostMapping("/check/username")
    public Boolean checkUsername(@RequestBody String username) {
        return defaultService.checkUsername(username);
    }

    @PutMapping("/{id}")
    public void updateUsers(@RequestBody User data, 
                            @PathVariable Long id) {
        defaultService.updateData(data, id);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable Long id) {
        defaultService.deleteData(id);
    }
}
