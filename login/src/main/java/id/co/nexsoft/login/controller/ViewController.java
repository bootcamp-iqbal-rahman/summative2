package id.co.nexsoft.login.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import id.co.nexsoft.login.service.LoginService;

@Controller
@RequestMapping("/")
public class ViewController {
    @Autowired
    private LoginService loginService;

    private static final String TAG = "message";
    private static final String[] MESSAGE = {
        "User not found", "Username or password incorrect", 
        "", "Internal server error"
    };
    
    @GetMapping
    public String index(ModelMap model) {
        model.put("message", "");
        return "index";
    }

    @PostMapping
    public String login(@RequestParam("username") String username,
                        @RequestParam("pass") String pass, ModelMap model) {
        int result = loginService.login(username, pass);
        model.put(TAG, MESSAGE[result]);
        return (result == 2) ? "success" : "index";
    }

    @GetMapping("/register")
    public String registerIndex(ModelMap model) {
        model.put(TAG, "");
        return "register";
    }

    @PostMapping("/register")
    public String register(
            @RequestParam(value = "firstname", required = false, defaultValue = "") String firstName,
            @RequestParam(value = "lastname", required = false, defaultValue = "") String lastName,
            @RequestParam(value = "email") String email,
            @RequestParam(value = "phone") String phone,
            @RequestParam(value = "birthdate", required = false, defaultValue = "2010-01-01") String birthDate,
            @RequestParam(value = "gender", required = false, defaultValue = "") String gender,
            @RequestParam("pass") String pass,
            ModelMap model) {
        String[] data = new String[7];
        data[0] = firstName;
        data[1] = lastName;
        data[2] = email;
        data[3] = phone;
        data[4] = birthDate;
        data[5] = gender;
        data[6] = pass;
        boolean isSuccess = loginService.register(data);

        model.put(TAG, (isSuccess)? "Register success, please login" : "Username or phone number already registered");
        return (isSuccess)? "index" : "register";
    }
}
