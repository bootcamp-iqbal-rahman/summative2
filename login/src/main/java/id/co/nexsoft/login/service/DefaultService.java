package id.co.nexsoft.login.service;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

public interface DefaultService<T> {
    List<T> getAllData();
    Optional<T> getDataById(Long id);
    Boolean checkUsername(String username);
    ResponseEntity<String> saveData(T data, BindingResult result);
    void updateData(T data, Long id);
    void deleteData(Long id);
}
