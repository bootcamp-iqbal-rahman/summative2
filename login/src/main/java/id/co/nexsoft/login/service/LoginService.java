package id.co.nexsoft.login.service;

public interface LoginService {
    int login(String username, String password);
    boolean register(String[] data);
}