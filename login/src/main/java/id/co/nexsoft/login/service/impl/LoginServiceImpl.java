package id.co.nexsoft.login.service.impl;

import java.net.http.HttpResponse;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import id.co.nexsoft.login.model.User;
import id.co.nexsoft.login.service.LoginService;
import id.co.nexsoft.login.util.FormatData;
import id.co.nexsoft.login.util.GetData;
import id.co.nexsoft.login.util.GetPassword;

@Service
public class LoginServiceImpl implements LoginService {

    @Override
    public int login(String username, String password) {
        HttpResponse<String> response = GetData.getData("api/user");
        List<User> people = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        int temp = 0;

        String usrName = cekUsername(username);

        try {
            JsonNode jsonNode = objectMapper.readTree(response.body());

            for (JsonNode personNode : jsonNode) {
                User person = objectMapper.treeToValue(personNode, User.class);
                people.add(person);
            }

            for (User usr : people) {
                if (usrName.equals(usr.getEmail()) || usrName.equals(usr.getPhone())) {
                    if (GetPassword.checkPassword(password, usr.getPassword())) {
                        return 2;
                    }
                    temp = 1;
                }
            }
            return temp;
        } catch (Exception e) {
            System.out.println("error : " + e.getMessage());
            return 3;
        }
    }

    public static String cekUsername(String username) {
        if (!FormatData.isValidEmail(username)) {
            return FormatData.formatPhoneNumber(username);
        }
        return username;
    }

    @Override
    public boolean register(String[] data) {
        String phoneNumber = FormatData.formatPhoneNumber(data[3]);
        HttpResponse<String> response = GetData.postSimpleData("api/user/check/username", data[2]);
        
        if (response.body().equals("true")) {
            HttpResponse<String> response2 = GetData.postSimpleData("api/user/check/username", phoneNumber);
            if (response2.body().equals("true")) {
                processRegister(data);
                return true;
            }
        }
        return false;
    }

    public void processRegister(String[] body) {
        SimpleDateFormat sdfOutput = new SimpleDateFormat("yyyy-MM-dd");
        LocalDate localDate = LocalDate.of(2010, 01, 01);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());

        try {
            Date date = sdfOutput.parse(body[4]);
            localDate = date.toInstant().atZone(java.time.ZoneId.systemDefault()).toLocalDate();

            User user =  new User(
                body[0],
                body[1],
                body[2],
                body[3],
                localDate,
                body[5],
                body[6]
            );

            String requestBody = objectMapper.writeValueAsString(user);
            GetData.putData("api/user", requestBody);
        } catch (Exception e) {

        }
    }
}
