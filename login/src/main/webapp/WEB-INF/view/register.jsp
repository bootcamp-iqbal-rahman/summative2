<!DOCTYPE html>
<html lang="en">
<head>
	<title>Register Page | Summative 2</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="icon" type="image/png" href="/images/icons/favicon.ico"/>
	<link rel="stylesheet" type="text/css" href="/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/fonts/iconic/css/material-design-iconic-font.min.css">
	<link rel="stylesheet" type="text/css" href="/vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="/vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="/vendor/animsition/css/animsition.min.css">
	<link rel="stylesheet" type="text/css" href="/vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="/vendor/daterangepicker/daterangepicker.css">
	<link rel="stylesheet" type="text/css" href="/css/util.css">
	<link rel="stylesheet" type="text/css" href="/css/main.css">
	<link rel="stylesheet" type="text/css" href="/css/style.css">
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('images/bg-01.jpg');">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">
				<form class="login100-form validate-form" action="/register" method="post">
					<span class="login100-form-title p-b-49">
						Register
					</span>
					<h6 class="allert-message">${message}</h6>
					<div class="wrap-input100 m-b-23">
						<span class="label-input100">First Name</span>
						<input class="input100" type="text" name="firstname" placeholder="Type your first name" autocomplete="off">
						<span class="focus-input100" data-symbol="&#xf206;"></span>
					</div>

					<div class="wrap-input100 m-b-23">
						<span class="label-input100">Last name</span>
						<input class="input100" type="text" name="lastname" placeholder="Type your last name" autocomplete="off">
						<span class="focus-input100" data-symbol="&#xf206;"></span>
					</div>

					<div class="wrap-input100 validate-input m-b-23" data-validate = "Email not valid">
						<span class="label-input100">Email</span>
						<input class="input100" type="text" name="email" placeholder="Type your email" autocomplete="off">
						<span class="focus-input100" data-symbol="&#xf206;"></span>
					</div>

					<div class="wrap-input100 validate-input m-b-23" data-validate = "Phone number is required">
						<span class="label-input100">Phone number</span>
						<input class="input100" type="text" name="phone" placeholder="Type your phone number" autocomplete="off">
						<span class="focus-input100" data-symbol="&#xf206;"></span>
					</div>

					<div class="wrap-input100 m-b-23">
						<span class="label-input100">Birth Date</span>
						<input class="input100" type="date" name="birthdate" autocomplete="off">
						<span class="focus-input100" data-symbol="&#xf206;"></span>
					</div>

					<div class="wrap-input100 m-b-23">
						<span class="label-input100">Gender</span>
						<select class="input100" name="gender">
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
						<span class="focus-input100" data-symbol="&#xf206;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Password is required">
						<span class="label-input100">Password</span>
						<input class="input100" type="password" id="password" name="pass" placeholder="Type your password" oninput="handlePasswordInput()" onblur="handlePasswordInput()">
						<span class="focus-input100" data-symbol="&#xf190;"></span>
						<span id="validation-message"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Password is required">
						<span class="label-input100">Retype password</span>
						<input class="input100" type="password" id="retype" name="pass2" placeholder="Type your password again" oninput="handleRetypeInput()" onblur="handleRetypeInput()">
						<span class="focus-input100" data-symbol="&#xf190;"></span>
						<span id="retype-message"></span>
					</div>
					
					<div class="text-right p-t-8 p-b-31">
						<a>Already have account?
							<a href="/"> Login Here</a>
						</a>
					</div>
					
					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn">
								Register
							</button>
						</div>
					</div>
					<input type="reset" value="Reset">
					<!-- <div class="flex-c-m">
						<a href="#" class="login100-social-item bg1">
							<i class="fa fa-facebook"></i>
						</a>

						<a href="#" class="login100-social-item bg2">
							<i class="fa fa-twitter"></i>
						</a>

						<a href="#" class="login100-social-item bg3">
							<i class="fa fa-google"></i>
						</a>
					</div> -->
				</form>
			</div>
		</div>
	</div>
	<!--===============================================================================================-->
	<script src="/vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="/vendor/animsition/js/animsition.min.js"></script>
	<script src="/vendor/bootstrap/js/popper.js"></script>
	<script src="/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="/vendor/select2/select2.min.js"></script>
	<script src="/vendor/daterangepicker/moment.min.js"></script>
	<script src="/vendor/daterangepicker/daterangepicker.js"></script>
	<script src="/vendor/countdowntime/countdowntime.js"></script>
	<script src="/js/main.js"></script>
	<script src="/js/pass.js"></script>
	<!--===============================================================================================-->
</body>
</html>