function validatePassword(password) {
    var passwordRegex = /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[^A-Za-z0-9]).{8,12}$/;
    return passwordRegex.test(password);
}

function handlePasswordInput() {
    var passwordInput = document.getElementById('password');
    var validationMessage = document.getElementById('validation-message');

    var isValid = validatePassword(passwordInput.value);

    validationMessage.textContent = isValid ? '' : 'Password must contain 1 Capital, 1 Special, alfabet and 1 Number';
}

function handleRetypeInput() {
    var passwordInput = document.getElementById('password');
    var retypeInput = document.getElementById('retype');
    var retypeMessage = document.getElementById('retype-message');

    var isMatch = (passwordInput.value === retypeInput.value);

    retypeMessage.textContent = isMatch ? '' : 'Passwords do not match';
}